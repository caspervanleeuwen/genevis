var DataHandlers = DataHandlers || {};

DataHandlers.BasicHandler = function(url, fileFormat, processData, delimiter, requestColumnsOfImportance){
    this.url = url;
    this.fileFormat = fileFormat;
    this.delimiter = delimiter;
    this.processData = processData;
    this.rawData = {};
    this.formattedData = {};
    this.requestColumnsOfImportance = requestColumnsOfImportance;
    this.dataHeader = [];
    this.columnsOfImportance = {};
    this.parse();
}

DataHandlers.BasicHandler.prototype = {
    parse : function(){
        if(this.delimiter != undefined){
            var dsv = d3.dsv(this.delimiter,"text/plain");
            dsv(this.url, this.saveData.bind(this))
        } 
        else{
            switch(this.fileFormat){
                case "tsv": d3.tsv(this.url, this.saveData.bind(this));
                break;
                case "csv": d3.csv(this.url, this.saveData.bind(this));
                break;
                default: d3.csv(this.url, this.saveData.bind(this));
            }
        }    
    },
    saveData : function(error, data){
        if(error != undefined){ 
            console.log(error);
            return;
        }
        this.dataHeader = Object.keys(data[0]);
        this.rawData = data;
        this.requestColumnsOfImportance.call(this);
    }
}

DataHandlers.ClusterDataHandler = function(url, fileFormat, requestColumnsOfImportance, progressFunction){
    this.columnsOfImportance = {"gene": "","entrezid": "","clusters":[]};
    DataHandlers.BasicHandler.call(this, url, fileFormat, this.processData, undefined, requestColumnsOfImportance);
    this.progressFunction = progressFunction;
}
DataHandlers.ClusterDataHandler.prototype = Object.create(DataHandlers.BasicHandler.prototype);
DataHandlers.ClusterDataHandler.constructor = DataHandlers.ClusterDataHandler;
DataHandlers.ClusterDataHandler.prototype.processData = function(){
    if(Object.keys(this.rawData).length == 0) return 0;
    var genes = this.rawData;

    resetScene();

    // Parsing clusters and user gene variables
    var userVariables = {};
    var clustersWithoutSpaces = {};
    //clusters = {};
    Object.keys(genes[0]).forEach(function(d){
      //if(d != this.columnsOfImportance.gene && d != this.columnsOfImportance.entrezid && this.columnsOfImportance.clusters.indexOf(d) == -1) userVariables[d] = {};
      userVariables[d.replace(/ /g, "_")] = {};
    }, this);

    var degreesPicker = shuffleArray(j$(1,360, this.columnsOfImportance.clusters.length)[0]);
    this.columnsOfImportance.clusters.forEach(function(cluster,i){
        clustersWithoutSpaces[cluster] = cluster.replace(/ /g, "_").replace("KEGG_","").toLowerCase();
        clusterGraph.info.clusterColors[clustersWithoutSpaces[cluster]] = d3.hsl(degreesPicker[i],0.5,0.5);
    },this)

    genes.forEach(function(gene,i,a){
        this.progressFunction((i/a.length)/3);
        //Adding cluster Graph nodes
        var associatedClusters = [];
        var linkedNodes = [];

        this.columnsOfImportance.clusters.forEach(function(cluster,i){
          var clusterAssociation = parseFloat(gene[cluster]);
          var clusterWithoutSpaces = clustersWithoutSpaces[cluster];
          if(clusterAssociation > clusterGraph.info.clusterAssociationTreshold){
            associatedClusters.push({"cluster":clusterWithoutSpaces, "association": clusterAssociation});
            var clusterNode = clusterGraph.getNodeById(clusterWithoutSpaces);
            if(clusterNode == undefined){
              clusterNode = clusterGraph.addNode(clusterWithoutSpaces,
                                                       undefined, 
                                                       {
                                                            "totalAssociation": clusterAssociation, 
                                                            "averageAssociation": 0,
                                                        }, 
                                                       {}, 
                                                       {"genes": {}});
            }
            else clusterNode.scores.totalAssociation += clusterAssociation;
            linkedNodes.push(clusterNode);
            clusterNode.data.genes[gene[this.columnsOfImportance.gene]] = clusterAssociation;
          }
        }, this);
        
        //Adding cluster Graph links
        var thisRoundOccured = {}; //Do to only get upper triange so occurences dont get doubled
        linkedNodes.forEach(function(sourceNode){
            linkedNodes.forEach(function(targetNode){
                var link = clusterGraph.getLinkBySourceTarget(sourceNode, targetNode);
                if(sourceNode.id != targetNode.id && (thisRoundOccured[sourceNode.id+targetNode.id] == undefined && thisRoundOccured[targetNode.id+sourceNode.id] == undefined)){
                    if(link == undefined){
                        link = clusterGraph.addLink(sourceNode,
                                                                              targetNode, 
                                                                              {"totalOccurences": 1},
                                                                              {}); //TODO Now its just number of overlap between clusters (Jacard) but must be similarity measure
                    }
                    else 
                        link.scores.totalOccurences++;
                    thisRoundOccured[sourceNode.id+targetNode.id] = link;
                }
            });
        });
        
        //Adding gene graph nodes
        var geneNode = geneGraph.getNodeById(gene[this.columnsOfImportance.gene]);
        if(geneNode == undefined){
            geneGraph.info.geneEntrezLookupTable[gene[this.columnsOfImportance.entrezid]] = gene[this.columnsOfImportance.gene];
            geneNode = geneGraph.addNode(gene[this.columnsOfImportance.gene], 
                                                                                  undefined, 
                                                                                  {}, 
                                                                                  {},
                                                                                  {    "clusters":associatedClusters, 
                                                                                        "traits":{},
                                                                                  }); 
            geneNode.data["geneEntrezId"] = gene[this.columnsOfImportance.entrezid];
            //Object.keys(userVariables).forEach(function(v){
            //    geneNode.data[v.replace(/\./g,"").toLowerCase()] = gene[v];
            //})
        } 
    }, this);
  
    //update average score cluster graph nodes
    clusterGraph.info.nodeMostGenes = new Graph.Node(undefined, undefined, {}, {}, {"genes":{}}, clusterGraph);
    Object.keys(clusterGraph.nodes).forEach(function(n,i,a){
        this.progressFunction((1/3)+(i/a.length)/3);
        var node = clusterGraph.nodes[n];
        var geneArr = Object.keys(node.data.genes).map(function(key){
            return node.data.genes[key];
        });
        //node.averageScore = node.totalScore/Object.keys(node.data.genes).length; //compute average
        geneArr.sort(function(a,b){
          if(a < b) return 1;
          if(a > b) return -1;
          return 0;});
        node.scores.averageAssociation = j$(geneArr.slice(0,15)).mean();
        if(Object.keys(node.data.genes).length > Object.keys(clusterGraph.info.nodeMostGenes.data.genes).length) clusterGraph.info.nodeMostGenes = node;
    },this);
    
    //Compute jacard
    Object.keys(clusterGraph.links).forEach(function(l,i,a){
        this.progressFunction((2/3)+(i/a.length)/3);
      var link = clusterGraph.links[l];
      var union = {};
      Object.keys(link.source.data.genes).forEach(function(p){
        union[p] = p;
      });
      Object.keys(link.target.data.genes).forEach(function(p){
        union[p] = p;
      });
      link.scores.jacard = link.scores.totalOccurences/Object.keys(union).length;
      if(link.scores.jacard > clusterGraph.info.clusterJacardMaxMin[1]) clusterGraph.info.clusterJacardMaxMin[1] = link.scores.jacard; 
      if(link.scores.jacard < clusterGraph.info.clusterJacardMaxMin[0]) clusterGraph.info.clusterJacardMaxMin[0] = link.scores.jacard;
    },this);
    
    //Updating scalers for cluster links
    clusterGraph.linkScoreScale.domain(clusterGraph.info.clusterJacardMaxMin);
    clusterGraph.linkScoreScale.domain(clusterGraph.info.clusterJacardMaxMin);

    UI.updateGeneQueryLists();

    return 1;
}

DataHandlers.EdgeDataHandler = function(url, fileFormat, requestColumnsOfImportance, progressFunction){
    DataHandlers.BasicHandler.call(this, url, fileFormat, this.processData, undefined, requestColumnsOfImportance);
    this.columnsOfImportance = {"source": "","target": "","scores":[]};
    this.progressFunction = progressFunction;
}
DataHandlers.EdgeDataHandler.prototype = Object.create(DataHandlers.BasicHandler.prototype);
DataHandlers.EdgeDataHandler.constructor = DataHandlers.EdgeDataHandler;
DataHandlers.EdgeDataHandler.prototype.processData = function(){
    if(Object.keys(this.rawData).length == 0) return 0;
    var edges = this.rawData;
    geneGraph.info.edgeScoresMaxMins = {}
    geneGraph.clearLinks();

    if(this.columnsOfImportance.scores.length != 0){
        this.columnsOfImportance.scores.forEach(function(score){
            geneGraph.info.edgeScoresMaxMins[score] = [Number.MAX_VALUE,Number.MIN_VALUE];
        })
    }
    else geneGraph.info.edgeScoresMaxMins["dummyScore"] = [Number.MAX_VALUE,Number.MIN_VALUE];
    //Adding gene graph links
    edges.forEach(function(edge,i,a){
        this.progressFunction((i/a.length));
        edgesourcename = edge[this.columnsOfImportance.source].replace("_HUMAN", "");
        edgetargetname = edge[this.columnsOfImportance.target].replace("_HUMAN", "");
        var sourceNode = geneGraph.getNodeById(isNaN(edgesourcename) ? edgesourcename : geneGraph.info.geneEntrezLookupTable[edgesourcename] ) ;
        var targetNode =  geneGraph.getNodeById(isNaN(edgetargetname) ?edgetargetname : geneGraph.info.geneEntrezLookupTable[edgetargetname]);
        var data = {};
        if(this.columnsOfImportance.scores.length != 0){
            this.columnsOfImportance.scores.forEach(function(score){
                data[score] = isNaN(edge[score])? 0: parseFloat(edge[score]);
                if(geneGraph.info.edgeScoresMaxMins[score][0] > data[score]) geneGraph.info.edgeScoresMaxMins[score][0] = data[score]; 
                if(geneGraph.info.edgeScoresMaxMins[score][1] < data[score]) geneGraph.info.edgeScoresMaxMins[score][1] = data[score];
            })
            geneGraph.selectedLinkScore = this.columnsOfImportance.scores[0];
        }
        else{
                data["dummyScore"] = 1;
                if(geneGraph.info.edgeScoresMaxMins["dummyScore"][0] > data["dummyScore"]) geneGraph.info.edgeScoresMaxMins["dummyScore"][0] = data["dummyScore"]; 
                if(geneGraph.info.edgeScoresMaxMins["dummyScore"][1] < data["dummyScore"]) geneGraph.info.edgeScoresMaxMins["dummyScore"][1] = data["dummyScore"];
            geneGraph.selectedLinkScore = "dummyScore";
        }
        if(sourceNode != undefined && targetNode != undefined && geneGraph.getLinkBySourceTarget(sourceNode, targetNode) == undefined && sourceNode.id != targetNode.id){
          geneGraph.addLink(sourceNode, targetNode, data, data); 
        }
    }, this);
    
    Object.keys(geneGraph.info.edgeScoresMaxMins).forEach(function(score){
        if(geneGraph.info.edgeScoresMaxMins[score][0] >= 0 && geneGraph.info.edgeScoresMaxMins[score][1] <= 1) geneGraph.info.edgeScoresMaxMins[score] = [0,1];    
    })
    return 1;
}


DataHandlers.DiseaseDataHandler = function(url, fileFormat, requestColumnsOfImportance, progressFunction){
    DataHandlers.BasicHandler.call(this, url, fileFormat, this.processData, undefined, requestColumnsOfImportance);
    this.columnsOfImportance = {"gene": "","trait":"", "score":""};
    this.progressFunction = progressFunction;
}
DataHandlers.DiseaseDataHandler.prototype = Object.create(DataHandlers.BasicHandler.prototype);
DataHandlers.DiseaseDataHandler.constructor = DataHandlers.DiseaseDataHandler;
DataHandlers.DiseaseDataHandler.prototype.processData = function(){
    if(Object.keys(this.rawData).length == 0) return 0;
    var geneClusterMaps = this.rawData;

    diseaseSets = {};
    Object.keys(geneGraph.nodes).forEach(function(g){
        var node = geneGraph.nodes[g];
        node.data.traits = {};
    })
    Object.keys(clusterGraph.nodes).forEach(function(c){
        var node = clusterGraph.nodes[c];
        node.data.traits = {};
    })

    // Add traits to associated nodes
    geneClusterMaps.forEach(function(entry,i,a){
        this.progressFunction((i/a.length)/2);
        this.columnsOfImportance.gene.forEach(function(geneColumn){
            var geneNames = parseGeneNames(entry[geneColumn]);
            if(geneNames != null){
                geneNames.forEach(function(geneName){
                    var geneNode = geneGraph.nodes[geneName];
                    var trait = entry[this.columnsOfImportance.trait];
                    if(diseaseSets[trait] == undefined) diseaseSets[trait] = {"genes":{},"clusters":{}};
                    if(geneNode != undefined && diseaseSets[trait]["genes"][geneName] == undefined){
                        var score = isNaN(entry[this.columnsOfImportance.score]) ? 0 : entry[this.columnsOfImportance.score] ;
                        diseaseSets[trait]["genes"][geneName] = score
                        geneNode.data.traits[trait] = score;
                        geneNode.data.clusters.forEach(function(c){
                                var cluster = clusterGraph.nodes[c.cluster];
                                if(cluster.data.traits == undefined) cluster.data["traits"] = {};
                                cluster.data.traits[trait] == undefined ? cluster.data.traits[trait] = {"total":1,"EASE":1} : cluster.data.traits[trait].total++;
                        })
                    }
                }, this)
            }
        },this)
    }, this)

    // Update cluster graph nodes with traits
    Object.keys(clusterGraph.nodes).forEach(function(n,i,a){
        this.progressFunction((1/2)+(i/a.length)/2);
        var clusterNode = clusterGraph.nodes[n];
        if(clusterNode.data.traits != undefined){
            Object.keys(clusterNode.data.traits).forEach(function(trait){
                    diseaseSets[trait].clusters[clusterNode.id] = {};
                    if(clusterNode.data.traits[trait] != undefined){
                        clusterNode.data.traits[trait].EASE = fisherExactTest(
                            clusterNode.data.traits[trait].total-1, 
                            Object.keys(clusterNode.data.genes).length - clusterNode.data.traits[trait].total,
                            Object.keys(diseaseSets[trait]["genes"]).length - clusterNode.data.traits[trait].total,
                            (Object.keys(geneGraph.nodes).length - Object.keys(clusterNode.data.genes).length)-(Object.keys(diseaseSets[trait]["genes"]).length - clusterNode.data.traits[trait].total)
                            ); 
                    }
                    else clusterNode.data.traits[trait].EASE = 1;
            });
        }
        else{
            clusterNode.data["traits"] = {};
        }
    },this)

    function parseGeneNames(entry){
        return entry != undefined ? entry.match(/[a-zA-Z0-9]+/g) : null;
    }

    function fisherExactTest(a,b,c,d){
        return jStat.hypgeom.pdf( a, a+b+c+d, a+b, a+c);
    }
    return 1;
}