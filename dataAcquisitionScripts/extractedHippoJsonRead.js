/*d3.json("proteinsv2.json", function(error, proteins) {
    Object.keys(proteins).forEach(function(p){
        var protein = proteins[p];
        //Building cluster Graph
        var linkedNodes = [];
        Object.keys(protein.clusterAssociations).forEach(function(c){
            var clusterAssociativeness = parseFloat(protein.clusterAssociations[c]);
            var clusterNode = clusterGraph.getNodeById(c);
            if(clusterNode == undefined){
                clusterNode = clusterGraph.addNodeSimple(c, undefined, clusterAssociativeness, 0, {}, clusterGraph);
                clusterNode.data.proteins = {};
            }
            if(clusterAssociativeness != 0){
                clusterNode.data.proteins[protein.UPId] = {"protein": protein, "associativeness": clusterAssociativeness};
                clusterNode.totalScore += clusterAssociativeness;
            }
            linkedNodes.push(clusterNode);
        });
        
        linkedNodes.forEach(function(sourceNode){
            linkedNodes.forEach(function(targetNode){
                if(clusterGraph.getLinkBySourceTarget(sourceNode, targetNode) == undefined &&
                   sourceNode.id != targetNode.id)
                    clusterGraph.addLink(sourceNode,targetNode, 0, 0, clusterGraph); //TODO find strength measure
            });
        });
        
        //Building protein graph
        var proteinNode = proteinGraph.getNodeById(p);
        if(proteinNode == undefined) proteinNode = proteinGraph.addNodeSimple(p, undefined, 0, 0, {}, proteinGraph);
        Object.keys(protein.interactions).forEach(function(interaction){
            var probability = parseFloat(protein.interactions[interaction]);
            if(probability != 0){
                var targetNode = proteinGraph.getNodeById(interaction);
                if(targetNode == undefined) targetNode = proteinGraph.addNodeSimple(interaction, undefined, 0, 0, {}, proteinGraph);
                if(proteinGraph.getLinkBySourceTarget(proteinNode, targetNode) == undefined)
                    proteinGraph.addLink(proteinNode, targetNode, probability, probability, proteinGraph);                
            }
        });
    });
    Object.keys(clusterGraph.nodes).forEach(function(n){
        var node = clusterGraph.nodes[n];
        node.averageScore = node.totalScore/Object.keys(node.data.proteins).length;
    });
    
    clusterGraph.updateStatistics();
    proteinGraph.updateStatistics();
    
    UI.initParameters(UI.clusterSVG);
    UI.updateStatistics();
    updateGraphVisuals(clusterGraph, UI.clusterSVG, clusterNodeAppender);
});*/