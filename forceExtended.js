/**
 * Extension of the force layou algorithm of d3.
 * It is not really made for extensions as it returns a object when called.
 * But by adding the attributes to the object we can simulate the extension.
 */




 d3.layout.forceExtended = function() {
    var force = d3.layout.force.call(this);
    var frozen = false; // EXTENSION frozen force state
    
    force.startIf = function(){
        if(frozen == true){
            return force.alpha() ? force.alpha(0).start() : force.start();
        }
        else{
            return force.alpha() ? force.alpha(0.3).start() : force.start();
        }
    };
   
    force.freeze = function(freeze){ // EXTENSION function to freeze the layout
        frozen = freeze; 
        if(frozen){
            return force.stop();
        } 
        else{
            return force.resume();
        }
    };

    return force;
  };
