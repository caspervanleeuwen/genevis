function shuffleArray(array) {
  var currentIndex = array.length, temporaryValue, randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
}

return array;
}

var getMinMaxNodes = function(graph, objectChain){
    var minScore = Number.MAX_VALUE; 
    var maxScore = Number.MIN_VALUE; 
    Object.keys(graph.nodes).forEach(function(d){
        var attr = graph.nodes[d]; 
        objectChain.forEach(function(d){
            if(attr != undefined) attr = attr[d];
        })
        if(attr != undefined){
            if(attr< minScore) 
                minScore = attr; 
            if(attr> maxScore) 
                maxScore = attr;
        }
    }); 
    return [minScore,maxScore];
}

var getMinMaxLinks = function(graph, objectChain){
    var minScore = Number.MAX_VALUE; 
    var maxScore = Number.MIN_VALUE; 
    Object.keys(graph.links).forEach(function(d){
        var attr = graph.links[d]; 
        objectChain.forEach(function(d){
            if(attr != undefined) attr = attr[d];
        })
        if(attr != undefined && attr[graph.selectedLinkScore] != undefined){
            if(attr[graph.selectedLinkScore] < minScore) 
                minScore = attr[graph.selectedLinkScore]; 
            if(attr[graph.selectedLinkScore] > maxScore) 
                maxScore = attr[graph.selectedLinkScore];
        }
    }); 
    return [minScore,maxScore];
}

/*var scoreQuery = function(obj, property){
    var remainingObj = [obj];
    while(remainingObj.length > 0){
        var currentObj = remainingObj.pop();
        if(typeof currentObj)
    }
    return undefined;
}*/