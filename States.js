var States = States || {};

/**
 * Represents the visual state of a graph
 * @constructor
 * @param {d3.scale} linkColorMap - Colormap to determine the color of the links in the graph
 * @param {d3.scale} linkScaleMap - Scalemap to determine the size of the links in the graph
 * @param {d3.scale} nodeColorMap - Colormap to determine the color of the nodes in the graph
 * @param {d3.scale} nodeScaleMap - Scalemap to determine the size of the nodes in the graph
 * @param {d3.layout.force} force - The force directed algorithm for the graph
 */
States.GraphVisualStates = function(linkColorMap, linkScaleMap, nodeColorMap, nodeScaleMap, force){
    //Init states if provided
    this.currentState = "default";
    this.currentLinkColorMap = linkColorMap;
    this.currentLinkScaleMap = linkScaleMap;
    this.currentNodeColorMap = nodeColorMap;
    this.currentNodeScaleMap = nodeScaleMap;
    this.currentForceLayout = force;
    
    //Color maps
    var linkColorMaps = {"default": linkColorMap};
    var linkScaleMaps = {"default": linkScaleMap};

    //Scale maps
    var nodeColorMaps = {"default": nodeColorMap};
    var nodeScaleMaps = {"default": nodeScaleMap};

    //Force state
    var forceLayouts = {"default": force};

    this.addVisualState = function(stateName, linkColorMap, linkScaleMap, nodeColorMap, nodeScaleMap, force){
        //Color maps
        linkColorMaps[stateName] = linkColorMap;
        linkScaleMaps[stateName] = linkScaleMap;

        //Scale maps
        nodeColorMaps[stateName] = nodeColorMap;
        nodeScaleMaps[stateName] = nodeScaleMap;

        //Force state
        forceLayouts[stateName] = force;
    }

    this.setCurrentVisualState = function(stateName){
        this.currentState = stateName;
        this.currentLinkColorMap = linkColorMaps[stateName];
        this.currentLinkScaleMap = linkScaleMaps[stateName];
        this.currentNodeColorMap = nodeColorMaps[stateName];
        this.currentNodeScaleMap = nodeScaleMaps[stateName];
        this.currentForceLayout = forceLayouts[stateName];
    }

    this.getCurrentVisualState = function(){
        return this.currentState;
    }
} 

/**
 * Container for all the maps for easy accessibility and for some occasions 
 * the only way to access them as they are located in internal functions
 */
 States.maps = {};

/**
 * Individual visual state initializations
 */
States.initGraphVisualStates = function(){
    States.initClusterGraphVisualStates();
    States.initSubGeneGraphVisualStates();

    clusterGraph.visualState.setCurrentVisualState("default");
    subGeneGraph.visualState.setCurrentVisualState("default");
}


States.initClusterGraphVisualStates = function(){
    var linkColorMap;
    var linkScaleMap;
    var nodeColorMap;
    var nodeScaleMap;
    var forceLayout;

    //Force Layout
    forceLayout = d3.layout.forceExtended()
        .gravity(0.05)
        .friction(0.7)
        .linkDistance(50);

    forceLayout.chargeScale = d3.scale.linear()
        .domain([0,1])
        .range([-3000, -25000]);
    
    forceLayout.dynamicCharge = function(d){
        return forceLayout.chargeScale(Object.keys(d.graph.nodes[d.id].data.genes).length/Object.keys(clusterGraph.info.nodeMostGenes.data.genes).length);
    };

    forceLayout.staticCharge = -5000;
    forceLayout.selectedCharge = forceLayout.dynamicCharge;

    forceLayout.dynamicLinkStrength = function(d){
        var link = d.graph.links[d.id];
        return link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore]);
    }

    forceLayout.staticLinkStrength = 0.1;
    forceLayout.selectedLinkStrength = forceLayout.dynamicLinkStrength;

    forceLayout
        .charge(forceLayout.selectedCharge)
        .linkStrength(forceLayout.selectedLinkStrength);

    clusterGraph.visualState = new States.GraphVisualStates();
    defaultState();
    diseaseState();

    /****************DEFAULT*STATE********************/
    function defaultState(){
        //Link color
        linkColorMap = d3.scale.linear()
            .domain([0, 1])
            .range(["#f7fbff", "#08306b"]);

        //Link scale
        linkScaleMap = d3.scale.linear()
            .domain([0,1])
            .range([1, 10]);

        //Node color
        nodeColorMap = function(cluster){
            return clusterGraph.info.clusterColors[cluster.id];
        }

        //Node scale
        var ellipseSizeScale= d3.scale.linear()
            .domain([0,1])
            .range([5, 500]);

        var ellipseAxisScale= d3.scale.linear()
           .domain([0,1])
           .range([5, 1]);

        nodeScaleMap = {
            "ellipseSizeScale": ellipseSizeScale, 
            "ellipseMajorAxisScale": function(sizeScore, ellipseScore){ return ~~Math.sqrt(ellipseSizeScale(sizeScore))*ellipseAxisScale(ellipseScore); },
        };

        //Add maps to maps container for quick accessibility
        States.maps["clusterDefaultLinkColorMap"] = linkColorMap;
        States.maps["clusterDefaultLinkScaleMap"] = linkScaleMap;
        States.maps["clusterDefaultEllipseSizeMap"] = ellipseSizeScale;
        States.maps["clusterDefaultEllipseAxisMap"] = ellipseAxisScale;

        //Add state
        clusterGraph.visualState.addVisualState("default", linkColorMap, linkScaleMap, nodeColorMap, nodeScaleMap, forceLayout);
    }

    /****************DISEASE*STATE********************/
    function diseaseState(){
         //Link color
        linkColorMap = d3.scale.linear()
            .domain([0, 1])
            .range(["#c7e9c0", "#00441b"]);

        //Link scale
        linkScaleMap = d3.scale.linear()
            .domain([0,1])
            .range([1, 10]);

        //Node color
        var nodeColorScale = d3.scale.linear()
            .domain([0, 1])
            .range(["#fff5f0", "#67000d"]);

        nodeColorMap = function(cluster){
            if(cluster.data.traits != undefined){
                if(cluster.data.traits[cluster.graph.selectedNodeScore.color] != undefined){
                    var p = cluster.data.traits[cluster.graph.selectedNodeScore.color].EASE;
                    var p = isNaN(p)? undefined:p; 
                    if(p <= 0.05) return "#67000d";
                    if(p > 0.05 && p <= 0.1) return "#fb6a4a";
                    if(p > 0.1) return "#fff5f0";
                }
            }
            return "#efedf5";
        }

        // nodeColorMap = function(cluster){
        //     if(cluster.data.traits != undefined){
        //         if(cluster.data.traits[cluster.graph.selectedNodeScore.color] != undefined)
        //             return nodeColorScale(cluster.data.traits[cluster.graph.selectedNodeScore.color].EASE);
        //     }
        //     return "#ffffff";
        // }

        //Node scale
        var ellipseSizeScale= d3.scale.linear()
            .domain([0,1])
            .range([5, 500]);

        var ellipseAxisScale= d3.scale.linear()
           .domain([0,1])
           .range([10, 1]);

        nodeScaleMap = {
            "ellipseSizeScale": ellipseSizeScale, 
            "ellipseAxisScale": function(sizeScore, ellipseScore){ return ellipseSizeScale(sizeScore)*ellipseAxisScale(ellipseScore); },
        };

        //Add maps to maps container for quick accessibility
        States.maps["clusterDiseaseLinkColorMap"] = linkColorMap;
        States.maps["clusterDiseaseLinkScaleMap"] = linkScaleMap;
        States.maps["clusterDiseaseEllipseSizeMap"] = ellipseSizeScale;
        States.maps["clusterDiseaseEllipseAxisMap"] = ellipseAxisScale;
        States.maps["clusterDiseaseNodeColorMap"] = nodeColorScale;

        //Add state
        clusterGraph.visualState.addVisualState("disease", linkColorMap, linkScaleMap, nodeColorMap, nodeScaleMap, forceLayout);
    }
}

States.initSubGeneGraphVisualStates = function(){
    var linkColorMap;
    var linkScaleMap;
    var nodeColorMap;
    var nodeScaleMap;
    var forceLayout;

    //Force layout
    forceLayout = d3.layout.forceExtended()
        .gravity(0.05)
        .friction(0.8)
        .linkDistance(50);

    forceLayout.chargeScale = d3.scale.linear()
        .domain([0,1])
        .range([-30, -3000]);
    
    forceLayout.dynamicCharge = function(d){
        var node = d.graph.nodes[d.id];
        return forceLayout.chargeScale(node.scores[node.graph.selectedNodeScore]);
    };

    forceLayout.highlightCharge = function(nodeData){
        return function(d){
            if(nodeData.indexOf(d.id) != -1) return -45000;
            else return -1000;
        }
    };

    forceLayout.staticCharge = -10000;
    forceLayout.selectedCharge = forceLayout.staticCharge;

    forceLayout.dynamicLinkStrength = function(d){
        var link = d.graph.links[d.id];
        return link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore])*(2/3);
    };

    forceLayout.highlightLinkStrength = function(linkData){
        return function(d){
            var link = d.graph.links[d.id];
            if(linkData.indexOf(link.id) != -1) return link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore])*(2/3);
            else return 0;
        }
    };

    forceLayout.staticLinkStrength = 0.1;
    forceLayout.selectedLinkStrength = forceLayout.dynamicLinkStrength;

    forceLayout
        .charge(forceLayout.selectedCharge)
        .linkStrength(forceLayout.selectedLinkStrength);

    subGeneGraph.visualState = new States.GraphVisualStates();
    defaultState();
    diseaseState();

    /****************DEFAULT*STATE********************/
    function defaultState(){
        //Link color
        linkColorMap = d3.scale.linear()
            .domain([0, 1])
            .range(["#f7fbff", "#08306b"]);

        //Link scale
        linkScaleMap = d3.scale.linear()
            .domain([0,1])
            .range([1, 10]);

        //Node color
        nodeColorMap = function(cluster){
            return clusterGraph.info.clusterColors[clusterGraph.nodes[cluster].id];
        }

        //Node scale
        nodeScaleMap = d3.scale.linear()
            .domain([0,1])
            .range([2.5, 250]);

        //Add maps to maps container for quick accessibility
        States.maps["clusterDefaultLinkColorMap"] = linkColorMap;
        States.maps["clusterDefaultLinkScaleMap"] = linkScaleMap;
        States.maps["clusterDefaultNodeScaleMap"] = nodeScaleMap;


        //Add state
        subGeneGraph.visualState.addVisualState("default", linkColorMap, linkScaleMap, nodeColorMap, nodeScaleMap, forceLayout);
    }

    /****************DISEASE*STATE********************/
    function diseaseState(){
         //Link color
        linkColorMap = d3.scale.linear()
            .domain([0, 1])
            .range(["#f7fbff", "#08306b"]);

        //Link scale
        linkScaleMap = d3.scale.linear()
            .domain([0,1])
            .range([1, 10]);

        /*//############NEW TEST#############

        //Node color
        var nodeColorScale = d3.scale.linear()
            .domain([0, 1])
            .range(["#67000d", "#fff5f0"]);

        nodeColorMap = function(){
            var p = this.parentElement.__data__.data.traits[this.parentElement.__data__.graph.selectedNodeScore.color];
            var p = isNaN(p)? undefined:p; 
            return nodeColorScale(p);
        }

        //############END NEW TEST##########*/

        //###########OLD KEEP#############
        //Node color
        nodeColorMap = function(){
            var p = this.parentElement.__data__.graph.nodes[this.parentElement.__data__.id].data.traits[this.parentElement.__data__.graph.selectedNodeScore.color];
            var p = isNaN(p)? undefined:p; 
            if(p <= 0.05) return "#67000d";
            if(p > 0.05 && p <= 0.1) return "#fb6a4a";
            if(p > 0.1) return "#fff5f0";
            return "#efedf5";
        }
        //###########END OLD KEEP#############

        //Node scale
        nodeScaleMap = d3.scale.linear()
            .domain([0,1])
            .range([2.5, 250]);

        //Add maps to maps container for quick accessibility
        States.maps["clusterDefaultLinkColorMap"] = linkColorMap;
        States.maps["clusterDefaultLinkScaleMap"] = linkScaleMap;
        States.maps["clusterDefaultNodeScaleMap"] = nodeScaleMap;

        //Add state
        subGeneGraph.visualState.addVisualState("disease", linkColorMap, linkScaleMap, nodeColorMap, nodeScaleMap, forceLayout);
    }
}